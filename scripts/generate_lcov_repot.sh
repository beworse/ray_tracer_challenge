#!/usr/bin/env bash

if [ -z "${PROJECT_DIR}" ] ; then
    echo "Please set variable \$\{PROJECT_DIR\}"
    echo "FOR example: export PROJECT_DIR=\$(pwd)"
    exit 1
fi

export BUILD_DIR="${PROJECT_DIR}/build"
if [ -z "${COVERAGE_DIR}" ] ; then
    export COVERAGE_DIR="${BUILD_DIR}/coverage"
fi

${BUILD_DIR}/tests

cd ${BUILD_DIR}/CMakeFiles/
INFO="results.info"
lcov --directory . --capture --output-file ${INFO}
lcov --remove ${INFO} "/usr/*" --output-file ${INFO}
genhtml ${INFO} -o ${COVERAGE_DIR}
echo "Coverage raprot generated ad ${COVERAGE_DIR}/index.html"