#!/usr/bin/env bash

if [ -z "${PROJECT_DIR}" ] ; then
    echo "Please set variable \$\{PROJECT_DIR\}"
    echo "FOR example: export PROJECT_DIR=\$(pwd)"
    exit 1
fi

export BUILD_DIR="${PROJECT_DIR}/build"



#if [ -d ${BUILD_DIR} ] ; then
#    rm -rf ${BUILD_DIR}
#fi

mkdir -pv ${BUILD_DIR}
cmake -S ${PROJECT_DIR} -B ${BUILD_DIR} -DBUILD_FOR_TESTS=1
cd ${BUILD_DIR} 
make

