################################################################################
# FUNCTIONS
################################################################################
function(compile_tests)
    include_directories(
        ${PROJECT_SOURCE_DIR}/tests
    )

    download_and_compile_google_tests()

    # remove main.cpp from list to prevent multidefined main function
    list(REMOVE_ITEM main_app_src "${PROJECT_SOURCE_DIR}/src/main.cpp")

    file(GLOB test_app_src
        "${PROJECT_SOURCE_DIR}/tests/ut/*.hpp"
        "${PROJECT_SOURCE_DIR}/tests/ut/*.cpp"
    )

    add_executable(tests ${test_app_src} ${main_app_src})
    target_link_libraries(tests gtest gtest_main)

    # restore item
    list(APPEND main_app_src "${PROJECT_SOURCE_DIR}/src/main.cpp")
endfunction()

function(download_and_compile_google_tests)

    # Enable ExternalProject CMake module
    include(ExternalProject)

    # Download and install GoogleTest
    ExternalProject_Add(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG release-1.8.1
        PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/third_party/googletest
        CONFIGURE_COMMAND   ""
        BUILD_COMMAND       ""
        INSTALL_COMMAND     ""
        TEST_COMMAND        ""
    )

    ExternalProject_Get_Property(googletest source_dir)
    set(GTEST_INLCUDE_DIRS ${source_dir}/googletest/include)

    ExternalProject_Get_Property(googletest binary_dir)
    set(GTEST_LIBS_DIR ${binary_dir}/googlmock/gtest)
endfunction()

################################################################################
# MAIN
################################################################################
cmake_minimum_required(VERSION 3.2)
project(ray_tracer_challenge)

# OPTIONAL FLAGS:
#set(BUILD_FOR_TESTS 1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -Wall -Wextra")

include_directories(
        ${PROJECT_SOURCE_DIR}/include
        ${PROJECT_SOURCE_DIR}/src        
)

file(GLOB main_app_src
    "${PROJECT_SOURCE_DIR}/src/*.hpp"
    "${PROJECT_SOURCE_DIR}/src/*.cpp"
)

if(${BUILD_FOR_TESTS})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -fprofile-arcs -ftest-coverage -BUILD_FOR_TESTS")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pthread")
    set(COVERAGE_EXCLUDES "/usr/local/include/gtest" "/usr/local/include/gtest/internal") 
    compile_tests()    
endif()

add_executable(app ${main_app_src})
