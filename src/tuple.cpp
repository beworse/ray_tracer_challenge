#include "tuple.hpp"
#include "common_macros.hpp"

#include <iostream>
#include <cmath>

Tuple::Tuple(float x, float y, float z, float w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

bool Tuple::isPoint()
{
    return epsilon_equal(1.f, w);
}

bool Tuple::isVector()
{
    return epsilon_equal(0.f, w);
}

float Tuple::magnitude()
{
    return std::sqrt((x * x) + (y * y) + (z * z));
}

void Tuple::normalize()
{
    float m = magnitude();
    x /= m;
    y /= m;
    z /= m;
    w = 0.f;
}

Tuple Tuple::normalized()
{
    float m = this->magnitude();
    return Tuple( x/m , y/m, z/m, 0.f);
}

float Tuple::cross(const Tuple &tuple)
{
    return 1.f;
}

Tuple Tuple::dot(const Tuple &tuple)
{
    return Tuple(1.f, 1.f, 1.f, 1.f);
}

//------------------------------------------------------------------------------
// Operators
//------------------------------------------------------------------------------
Tuple Tuple::operator-()
{
    return Tuple(
        -x,
        -y,
        -z,
        -w
    );
}

Tuple Tuple::operator-(const Tuple &tuple)
{
    return Tuple (
        x - tuple.x,
        y - tuple.y,
        z - tuple.z,
        w - tuple.w
    );
}

Tuple Tuple::operator+(const Tuple &tuple)
{
    return Tuple (
        x + tuple.x,
        y + tuple.y,
        z + tuple.z,
        w + tuple.w
    );
}


Tuple Tuple::operator*(float scalar)
{
    return Tuple (
        x * scalar,
        y * scalar,
        z * scalar,
        w * scalar
    );
}

Tuple Tuple::operator/(float scalar)
{
    return Tuple (
        x / scalar,
        y / scalar,
        z / scalar,
        w / scalar
    );
}

bool operator==(const Tuple &lhs, const Tuple &rhs)
{
    return epsilon_equal(lhs.x, rhs.x)
       and epsilon_equal(lhs.y, rhs.y)
       and epsilon_equal(lhs.z, rhs.z)
       and epsilon_equal(lhs.w, rhs.w);
}

bool operator!=(const Tuple &lhs, const Tuple &rhs)
{
    return not(lhs == rhs);
}

std::ostream& operator<<(std::ostream &os, const Tuple &tuple)
{
    os << "[" << tuple.x << ", " << tuple.y << ", " << tuple.z << ", "
      << tuple.w << "]" ;
    return os;
}
