#pragma once
#include <iostream>

class Tuple
{
    public:
        float x;
        float y;
        float z;
        float w;

        Tuple(float x, float y, float z, float w);

        bool isPoint();
        bool isVector();

        float magnitude();

        void normalize();
        Tuple normalized();

        float cross(const Tuple &tuple);
        Tuple dot(const Tuple &tuple);

        Tuple operator-();
        Tuple operator-(const Tuple &tuple);
        Tuple operator+(const Tuple &tuple);
        Tuple operator*(float scalar);
        Tuple operator/(float scalar);

        friend std::ostream& operator<<(std::ostream &os, const Tuple &tuple);
};

bool operator==(const Tuple &lhs, const Tuple &rhs);
bool operator!=(const Tuple &lhs, const Tuple &rhs);

// I don't see reason to inherit
#define Vector(x,y,z) Tuple(x, y, z, 0.f)
#define Point(x,y,z) Tuple(x, y, z, 1.f)
//#define Color(r,g,b) Tuple(r, g, b, 1.f)
