#include <iostream>
#include "tuple.hpp"

int main()
{
    auto *tmp = new Tuple(1.f , 1.f , 1.f , false);
    std::cout << "Tuple: " << *tmp << std::endl
        << *tmp + *tmp << *tmp << std::endl;
    delete tmp;

    return 0;
}