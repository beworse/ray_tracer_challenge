#pragma once

constexpr float EPSILON = {0.000001};

bool epsilon_equal(float a, float b)
{
    float tmp = a - b;
    return (-EPSILON <= tmp) and (tmp <= EPSILON);
}
