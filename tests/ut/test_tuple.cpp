#include "tuple.hpp"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <sstream>
#include <cmath>

TEST(tuple, create_tuple_vector)
{
    Tuple tuple = Tuple(4.3f, -4.2f, 3.1f, 1.f);
    ASSERT_FLOAT_EQ(tuple.x, 4.3f);
    ASSERT_FLOAT_EQ(tuple.y, -4.2f);
    ASSERT_FLOAT_EQ(tuple.z, 3.1f);
    ASSERT_TRUE(tuple.isPoint());
    ASSERT_FALSE(tuple.isVector());
}

TEST(tuple, create_tuple_point)
{
    Tuple tuple = Tuple(4.3f, -4.2f, 3.1f, 0.f);
    ASSERT_FLOAT_EQ(tuple.x, 4.3f);
    ASSERT_FLOAT_EQ(tuple.y, -4.2f);
    ASSERT_FLOAT_EQ(tuple.z, 3.1f);
    ASSERT_FALSE(tuple.isPoint());
    ASSERT_TRUE(tuple.isVector());
}

TEST(tuple, create_point)
{
    Tuple tuple = Point(4.3f, -4.2f, 3.1f);
    ASSERT_FLOAT_EQ(tuple.x, 4.3f);
    ASSERT_FLOAT_EQ(tuple.y, -4.2f);
    ASSERT_FLOAT_EQ(tuple.z, 3.1f);
    ASSERT_TRUE(tuple.isPoint());
    ASSERT_FALSE(tuple.isVector());
}

TEST(tuple, create_vector)
{
    Tuple tuple = Vector(4.3f, -4.2f, 3.1f);
    ASSERT_FLOAT_EQ(tuple.x, 4.3f);
    ASSERT_FLOAT_EQ(tuple.y, -4.2f);
    ASSERT_FLOAT_EQ(tuple.z, 3.1f);
    ASSERT_FALSE(tuple.isPoint());
    ASSERT_TRUE(tuple.isVector());
}

TEST(tuple, sum_tuples)
{
    Tuple a = Tuple( 3.f, -2.f, 5.f, 1.f);
    Tuple b = Tuple(-2.f,  3.f, 1.f, 0.f);
    Tuple c = a + b;
    ASSERT_FLOAT_EQ(c.x, 1.f);
    ASSERT_FLOAT_EQ(c.y, 1.f);
    ASSERT_FLOAT_EQ(c.z, 6.f);
    ASSERT_FLOAT_EQ(c.w, 1.f);
}

TEST(tuple, substract_points)
{
    Tuple a = Point( 3.f, 2.f, 1.f);
    Tuple b = Point( 5.f, 6.f, 7.f);
    Tuple c = a - b;
    ASSERT_FLOAT_EQ(c.x, -2.f);
    ASSERT_FLOAT_EQ(c.y, -4.f);
    ASSERT_FLOAT_EQ(c.z, -6.f);
    ASSERT_TRUE(c.isVector());
}

TEST(tuple, substract_vector_point)
{
    Tuple a = Point( 3.f, 2.f, 1.f);
    Tuple b = Vector( 5.f, 6.f, 7.f);
    Tuple c = a - b;
    ASSERT_FLOAT_EQ(c.x, -2.f);
    ASSERT_FLOAT_EQ(c.y, -4.f);
    ASSERT_FLOAT_EQ(c.z, -6.f);
    ASSERT_TRUE(c.isPoint());
}

TEST(tuple, substract_vectors)
{
    Tuple a = Vector( 3.f, 2.f, 1.f);
    Tuple b = Vector( 5.f, 6.f, 7.f);
    Tuple c = a - b;
    ASSERT_FLOAT_EQ(c.x, -2.f);
    ASSERT_FLOAT_EQ(c.y, -4.f);
    ASSERT_FLOAT_EQ(c.z, -6.f);
    ASSERT_TRUE(c.isVector());
}

TEST(tuple, substract_zero_vector)
{
    Tuple a = Vector( 0.f,  0.f, 0.f);
    Tuple b = Vector( 1.f, -2.f, 3.f);
    Tuple c = a - b;
    ASSERT_FLOAT_EQ(c.x, -1.f);
    ASSERT_FLOAT_EQ(c.y,  2.f);
    ASSERT_FLOAT_EQ(c.z, -3.f);
    ASSERT_TRUE(c.isVector());
}

TEST(tuple, negation_tuple)
{
    Tuple tuple = Tuple(1, -2, 3, -4);
    tuple = -tuple;
    ASSERT_FLOAT_EQ(tuple.x, -1.f);
    ASSERT_FLOAT_EQ(tuple.y,  2.f);
    ASSERT_FLOAT_EQ(tuple.z, -3.f);
    ASSERT_FLOAT_EQ(tuple.w,  4.f);
}

TEST(tuple, multiple_tuple_scalar)
{
    Tuple tuple = Tuple(1, -2, 3, -4);
    tuple = tuple * 3.5f;
    ASSERT_FLOAT_EQ(tuple.x,  3.5f);
    ASSERT_FLOAT_EQ(tuple.y, -7.f);
    ASSERT_FLOAT_EQ(tuple.z,  10.5f);
    ASSERT_FLOAT_EQ(tuple.w, -14.f);
}

TEST(tuple, multiple_tuple_fraction)
{
    Tuple tuple = Tuple(1, -2, 3, -4);
    tuple = tuple * 0.5f;
    ASSERT_FLOAT_EQ(tuple.x,  0.5f);
    ASSERT_FLOAT_EQ(tuple.y, -1.f);
    ASSERT_FLOAT_EQ(tuple.z,  1.5f);
    ASSERT_FLOAT_EQ(tuple.w, -2.f);
}

TEST(tuple, divide_tuple_scalar)
{
    Tuple tuple = Tuple(1, -2, 3, -4);
    tuple = tuple / 2;
    ASSERT_FLOAT_EQ(tuple.x,  0.5f);
    ASSERT_FLOAT_EQ(tuple.y, -1.f);
    ASSERT_FLOAT_EQ(tuple.z,  1.5f);
    ASSERT_FLOAT_EQ(tuple.w, -2.f);
}

TEST(tuple, magnitude_one_vectors)
{
    ASSERT_FLOAT_EQ(Vector(1.f, 0.f, 0.f).magnitude(), 1.f);
    ASSERT_FLOAT_EQ(Vector(0.f, 1.f, 0.f).magnitude(), 1.f);
    ASSERT_FLOAT_EQ(Vector(0.f, 0.f, 1.f).magnitude(), 1.f);
}

TEST(tuple, magnitude_vectors)
{
    ASSERT_FLOAT_EQ(Vector(1.f, 2.f, 3.f).magnitude(), std::sqrt(14));
    ASSERT_FLOAT_EQ(Vector(-1.f, -2.f, -3.f).magnitude(), std::sqrt(14));
}

TEST(tuple, normalize_vectors)
{
    Tuple *vector = new Vector(4.f, 0.f, 0.f);
    Tuple *result = new Vector(1.f, 0.f, 0.f);
    ASSERT_EQ( (*vector).normalized(), *result);
    vector->normalize();
    ASSERT_EQ(*vector, *result);

    constexpr float m = std::sqrt(14.f);
    vector = new Vector(1.f, 2.f, 3.f);
    result = new Vector(1.f/m, 2.f/m, 3.f/m);
    ASSERT_EQ( (*vector).normalized(), *result);
    vector->normalize();
    ASSERT_EQ(*vector, *result);

    delete vector;
    delete result;
}

TEST(tuple, normalized_vector_magnitude)
{
    Tuple vector = Vector(1.f, 2.f, 3.f);
    vector.normalize();
    ASSERT_FLOAT_EQ(vector.magnitude(), 1.f);
}

TEST(tuple, dot_product_vectors)
{
    Tuple vec1 = Vector(1.f, 2.f, 3.f);
    Tuple vec2 = Vector(2.f, 3.f, 4.f);
    ASSERT_FLOAT_EQ(vec1.cross(vec2), 20.f);
}

TEST(tuple, cross_product_vectors)
{
    Tuple vec1 = Vector(1.f, 2.f, 3.f);
    Tuple vec2 = Vector(2.f, 3.f, 4.f);
    ASSERT_EQ(vec1.dot(vec2), Vector(-1,  2, -1));
    ASSERT_EQ(vec2.dot(vec1), Vector( 1, -2,  1));
}

TEST(tuple, equality_tuples)
{
    ASSERT_EQ(Tuple(1.f, 2.f, 3.f, 0.f), Tuple(1.f, 2.f, 3.f, 0.f));
    ASSERT_NE(Tuple(3.f, 2.f, 1.f, 5.f), Tuple(1.f, 2.f, 3.f, 0.f));
}

TEST(tuple, ostream)
{
    Tuple a = Tuple( 1.f,  2.f, 3.f, 1.f);
    testing::internal::CaptureStdout();
    std::cout << a;
    std::string actual_result = testing::internal::GetCapturedStdout();

    std::ostringstream result;
    result << "[" << a.x << ", " << a.y <<", " << a.z << ", " << a.w << "]";

    ASSERT_EQ(actual_result, result.str());
}
